### Go to backend folder ###

* copy .env.eample to .env   (update the db name and db password)
* composer install
* php artisan migrate --seed
* php artisan serve    // be sure to run the application in localhost:8000

### Go to frontend folder ###

* copy .env.example to .env    (update all the variables)
* npm run install
* npm run dev  // be sure to run the application in localhost:3000


### User1 credentials ###
* Email: demo1@example.com
* Password: 12345678

### User2 credentials ###
* Email: demo2@example.com
* Password: 12345678

### User3 credentials ###
* Email: demo3@example.com
* Password: 12345678
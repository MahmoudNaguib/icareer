<?php
function token() {
    $token = request()->header('token');
    if (!$token) {
        $token = request()->header('Authorization');
    }
    if ($token) {
        $token = str_replace('Bearer ', '', $token);
    }
    return $token;
}


function configureUploads() {
    $uploads = public_path() . '/uploads';
    if (!File::isDirectory($uploads)) {
        File::makeDirectory($uploads, 0777, true, true);
    }
    $small = $uploads . '/small';
    if (!File::isDirectory($small)) {
        File::makeDirectory($small, 0777, true, true);
    }
    $large = $uploads . '/large';
    if (!File::isDirectory($large)) {
        File::makeDirectory($large, 0777, true, true);
    }
}

function insertDefaultConfigs() {
    \Cache::forget('configs');
    $logo = resizeImage(resource_path() . '/logo.png', ['small' => 'resize,200x200', 'large' => 'resize,500x500']);
    $rows = [];
    ///////////////// Logo
    $rows[] = [
        'field' => 'logo',
        'value' => $logo,
    ];
    //////////// appName
    $rows[] = [
        'field' => 'app_name',
        'value' => env('APP_NAME'),
    ];
    $rows[] = [
        'field' => 'address',
        'value' => env('CONTACT_ADDRESS'),
    ];
    $rows[] = [
        'field' => 'phone',
        'value' => env('CONTACT_PHONE'),
    ];

    $rows[] = [
        'field' => 'email',
        'value' => env('CONTACT_EMAIL'),
    ];
    $rows[] = [
        'field' => 'meta_description',
        'value' => env('META_DESCRIPTION'),
    ];

    $rows[] = [
        'field' => 'meta_keywords',
        'value' => env('META_KEYWORDS'),
    ];
    \DB::table('configs')->insert($rows);
}


function insertDefaultUsers() {
    ///////////// insert user 1
    $user = [
        'name' => 'Demo',
        'email' => 'demo1@example.com',
        'mobile' => '01234567890',
        'password' => bcrypt('12345678'),
        'confirmed' => 1,
    ];
    $hash = md5(time()) . RandomString(100);
    $id = \DB::table('users')->insertGetId($user);
    \App\Models\Token::create([
        'user_id' => $id,
        'token' => $hash . md5($id),
        'device' => 'web',
        'push_token' => 'push_token1',
    ]);
    ///////////// insert user 2
    $user = [
        'name' => 'Demo2',
        'email' => 'demo2@example.com',
        'mobile' => '01234567890',
        'password' => bcrypt('12345678'),
        'confirmed' => 1,
    ];
    $hash = md5(time()) . RandomString(100);
    $id = \DB::table('users')->insertGetId($user);
    \App\Models\Token::create([
        'user_id' => $id,
        'token' => $hash . md5($id),
        'device' => 'web',
        'push_token' => 'push_token2',
    ]);
    ///////////// insert user 3
    $user = [
        'name' => 'Demo3',
        'email' => 'demo3@example.com',
        'mobile' => '01234567890',
        'password' => bcrypt('12345678'),
        'confirmed' => 1,
    ];
    $hash = md5(time()) . RandomString(100);
    $id = \DB::table('users')->insertGetId($user);
    \App\Models\Token::create([
        'user_id' => $id,
        'token' => $hash . md5($id),
        'device' => 'web',
        'push_token' => 'push_token3',
    ]);
}

function movies() {
    return [
        '399566, Godzilla vs. Kong, /pgqgaUx1cJb5oZQQ5v0tNARCeBp.jpg',
        '527774, Raya and the Last Dragon, /lPsD10PP4rgUGiGR4CCXA6iY0QQ.jpg',
        '544401, Cherry, /pwDvkDyaHEU9V7cApQhbcSJMG1w.jpg',
        '587996, Below Zero, /dWSnsAGTfc8U27bWsy2RfwZs0Bs.jpg',
        '581389, Space Sweepers, /bmemsraCG1kIthY74NjDnnLRT2Q.jpg',
        '791373, Zack Snyder Justice League, /tnAuB8q5vv7Ax9UAEje5Xi4BXik.jpg',
        '412656, Chaos Walking, /9kg73Mg8WJKlB9Y2SAJzeDKAnuB.jpg',
        '587807, Tom & Jerry, /6KErczPBROQty7QoIsaa6wJYXZi.jpg',
        '458576, Monster Hunter, /1UCOF11QCw8kcqvce8LKOO6pimh.jpg',
        '587996, Below Zero, /dWSnsAGTfc8U27bWsy2RfwZs0Bs.jpg',
        '464052, Wonder Woman 1984, /8UlWHLMpgZm9bx6QYh0NFoq67TZ.jpg',
    ];
}

function insertDefaultFavourites() {
    $users=\App\Models\User::get();
    if($users){
        foreach ($users as $user){
            $movies = array_slice(movies(),0,rand(4,6));
            foreach ($movies as $movie) {
                $row = explode(', ', $movie);
                $data = [
                    'user_id' => $user->id,
                    'movie_id' => $row[0],
                    'movie_title' => $row[1],
                    'movie_img' => $row[2]
                ];
                \App\Models\Favourite::create($data);
            }
        }
    }
}

function insertDefaultٌRates() {
    $favourites=\App\Models\Favourite::get();
    if($favourites){
        foreach ($favourites as $favourite){
            $data = [
                'user_id' => $favourite->id,
                'movie_id' => $favourite->movie_id,
                'movie_title' => $favourite->movie_title,
                'movie_img' => $favourite->movie_img,
                'value'=>rand(1,5)
            ];
            \App\Models\Rate::create($data);
        }
    }
}





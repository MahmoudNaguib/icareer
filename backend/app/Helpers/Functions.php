<?php

use Intervention\Image\Facades\Image;

function uploads() {
    return 'uploads';
}
function RandomString($n) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $randstring = '';
    for ($i = 0; $i < $n; $i++) {
        $randstring .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randstring;
}

function resizeImage($image, $sizes = ['small' => 'resize,200x200']) {
    $fileName = pathinfo($image)['basename'];
    $image = Image::make($image);
    $newFileName = strtolower(str_random(10)) . time() . '.' . $image->extension;
    foreach ($sizes as $key => $size) {
        $uploadsPath = public_path() . '/' . uploads() . '/' . $key . '/' . $newFileName;
        $size = explode(',', $size);
        $type = $size[0];
        $dimensions = (isset($size[1])) ? $size[1] : '200x200';
        $dimensions = explode('x', $dimensions);
        if ($type == 'crop') {
            $image->fit($dimensions[0], $dimensions[1]);
        } else {
            $image->resize($dimensions[0], $dimensions[1], function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $image->save($uploadsPath);
    }
    return $newFileName;
}

function appVersion() {
    $json = json_decode(@file_get_contents(public_path() . '/version.json'));
    return @$json->version;
}
function transformValidation($errors) {
    $temp = [];
    if ($errors) {
        foreach ($errors as $key => $value) {
            $temp[$key] = @$value[0];
        }
    }
    return $temp;
}
function getConfigs() {
    $arr = [];
    if (\Schema::hasTable('configs')) {
        $configs = \App\Models\Config::get();
        if ($configs) {
            foreach ($configs as $c) {
                $key = $c->field;
                $arr[$key] = $c->value;
            }
        }
    }
    $arr['version']=appVersion();
    return $arr;
}

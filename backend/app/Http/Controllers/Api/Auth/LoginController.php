<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Token;
use Hash;
use Illuminate\Support\Facades\Validator;
use Auth;

class LoginController extends Controller {
    /*
     * 200: success
     * 201 created
     * 401: unauthorized
     * 404: page not found
     * 400: Bad Request
     * 422: Validation error
     * 403: Forbidden
     */

    public function __construct(\App\Models\User $model) {
        $this->model = $model;
        $this->loginRules = $model->loginRules;
    }
    public function store() {
        $validator = Validator::make(request()->all(), $this->loginRules);
        if ($validator->fails()) {
            $res['message'] = trans('api.Invalid input data');
            $res['errors'] = transformValidation($validator->errors()->messages());
            return response()->json($res, 422);
        }
        $row = $this->model->where('email', request('email'))->first();
        if (!$row) {
            $message = trans('api.There is no account with this email');
            return response()->json(['message' => $message], 403);
        }
        if (!$row->confirmed) {
            $message = trans('api.This account is not confirmed') . ', ' . trans('api.Please check your email to confirm your account');
            return response()->json(['message' => $message], 403);
        }
        if (!Hash::check(trim(request('password')), $row->password)) {
            $message = trans('api.Trying to login with invalid password');
            return response()->json(['message' => $message], 403);
        }
        ///////////////////////////////// Create token
        if (request('push_token')) {
            $token = Token::where('push_token', request('push_token'))->where('user_id', $row->id)->first();
        }
        if (!@$token) {
            $hash = md5(time()).RandomString(100).md5($row->id);
            $push_token=request('push_token');
            $token = \App\Models\Token::create([
                'user_id' => $row->id,
                'token' => $hash,
                'device' => request('device'),
                'push_token' => $push_token,
            ]);
        }
        //////////////////// Create new token
        $data = [
            'last_logged_in_at' => date("Y-m-d H:i:s"),
            'last_ip' => @$_SERVER['REMOTE_ADDR']
        ];
        if ($row->update($data)) {
            \Auth::login($row);
            request()->headers->set('token', $token->token);
            $row = $this->model
                ->findOrFail($row->id);
            return response()->json([
                'message' => trans('api.Successfully logged in'),
                'data' => new \App\Http\Resources\UserResource($row)
            ], 200);
        }
        return response()->json(['message' => trans('api.Failed to login')], 400);
    }

}

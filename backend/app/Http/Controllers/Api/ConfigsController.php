<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Token;
use Hash;
use Validator;

class ConfigsController extends Controller {
    /*
     * 200: success
     * 201 created
     * 401: unauthorized
     * 404: page not found
     * 400: Bad Request
     * 422: Validation error
     * 403: Forbidden
     */

    public $model;

    public function __construct(\App\Models\Config $model) {
        $this->model = $model;
    }

    public function index() {
        $configs=$this->model->getConfigs();
        return response()->json(['data' => $configs], 200);
    }
}

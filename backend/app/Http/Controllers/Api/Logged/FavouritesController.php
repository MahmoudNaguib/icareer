<?php

namespace App\Http\Controllers\Api\Logged;

use App\Http\Controllers\Controller;
use App\Models\Token;
use Hash;
use Validator;

class FavouritesController extends Controller {
    /*
     * 200: success
     * 201 created
     * 401: unauthorized
     * 404: page not found
     * 400: Bad Request
     * 422: Validation error
     * 403: Forbidden
     */

    public $model;

    public function __construct(\App\Models\Favourite $model) {
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function index() {
        $rows = $this->model->own()->get();
        return \App\Http\Resources\FavouritesResource::collection($rows);
    }

    public function show($id) {
        $row = $this->model->own()->findOrFail($id);
        return new \App\Http\Resources\FavouritesResource($row);
    }

    public function store() {
        request()->request->add(['user_id' => auth()->user()->id]);
        $validator = Validator::make(request()->all(), $this->rules);
        if ($validator->fails()) {
            $res['message'] = trans('api.Invalid input data');
            $res['errors'] = transformValidation($validator->errors()->messages());
            return response()->json($res, 422);
        }
        $row = $this->model
            ->where('movie_id', request('movie_id'))
            ->where('user_id', auth()->user()->id)
            ->first();
        if ($row) {
            return response()->json([
                'message' => trans('api.This movie is already in your favourites'),
            ], 200);
        }
        if ($row = $this->model->create(request()->all())) {
            return response()->json([
                'message' => trans("api.Added successfully"),
                'data'=>new \App\Http\Resources\FavouritesResource($row)
            ], 201);
        }
    }

    public function destroy($id) {
        $row = $this->model->own()->findOrFail($id);
        if ($row->delete()) {
            return response()->json([
                'message' => trans('api.Deleted successfully'),
            ], 200);
        }
        return response()->json(['message' => trans('api.Failed to delete')], 400);
    }


}

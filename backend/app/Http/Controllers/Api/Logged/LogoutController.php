<?php

namespace App\Http\Controllers\Api\Logged;

use App\Http\Controllers\Controller;
use App\Models\Token;
use Hash;
use Validator;

class LogoutController extends Controller {
    /*
     * 200: success
     * 201 created
     * 401: unauthorized
     * 404: page not found
     * 400: Bad Request
     * 422: Validation error
     * 403: Forbidden
     */

    public $model;

    public function __construct(\App\Models\User $model) {
        $this->model = $model;
    }

    public function index() {
        $token = Token::where('user_id', auth()->user()->id)->where('token', token())->delete();
        auth()->logout();
        return response()->json(['message' => trans('api.Log out successfully')], 200);
    }

}

<?php

namespace App\Http\Controllers\Api\Logged;

use App\Http\Controllers\Controller;
use App\Models\Token;
use Hash;
use Validator;

class ProfileController extends Controller {
    /*
     * 200: success
     * 201 created
     * 401: unauthorized
     * 404: page not found
     * 400: Bad Request
     * 422: Validation error
     * 403: Forbidden
     */

    public $model;

    public function __construct(\App\Models\User $model) {
        $this->model = $model;
    }

    public function index() {
        return new \App\Http\Resources\UserResource($this->model->findOrFail(auth()->user()->id));
    }
}

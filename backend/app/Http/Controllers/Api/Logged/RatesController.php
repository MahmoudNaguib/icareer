<?php

namespace App\Http\Controllers\Api\Logged;

use App\Http\Controllers\Controller;
use App\Models\Token;
use Hash;
use Validator;

class RatesController extends Controller {
    /*
     * 200: success
     * 201 created
     * 401: unauthorized
     * 404: page not found
     * 400: Bad Request
     * 422: Validation error
     * 403: Forbidden
     */

    public $model;

    public function __construct(\App\Models\Rate $model) {
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function index() {
        $rows = $this->model->own()->get();
        return \App\Http\Resources\RatesResource::collection($rows);
    }

    public function show($id) {
        $row = $this->model->own()->findOrFail($id);
        return new \App\Http\Resources\RatesResource($row);
    }

    public function store() {
        request()->request->add(['user_id' => auth()->user()->id]);
        $validator = Validator::make(request()->all(), $this->rules);
        if ($validator->fails()) {
            $res['message'] = trans('api.Invalid input data');
            $res['errors'] = transformValidation($validator->errors()->messages());
            return response()->json($res, 422);
        }
        $row = $this->model
            ->where('movie_id', request('movie_id'))
            ->where('user_id', auth()->user()->id)
            ->first();

        $favouriteExist=\App\Models\Favourite::where('movie_id', request('movie_id'))
            ->where('user_id', auth()->user()->id)
            ->first();
        if(!$favouriteExist){
            $data=[
                'movie_id'=>request('movie_id'),
                'movie_title'=>request('movie_title'),
                'movie_img'=>request('movie_img'),
                'user_id'=>auth()->user()->id,
            ];
            $favourite=\App\Models\Favourite::create($data);
        }
        if ($row) {
            $response['message']=trans('api.This movie is already rated');
        }
        if ($row = $this->model->create(request()->all())) {
            $response['message']=trans('api.Rated successfully');
            $response['data']=new \App\Http\Resources\RatesResource($row);
            if(@$favourite){
                $response['favourite']=new \App\Http\Resources\FavouritesResource($favourite);
            }
        }
        return response()->json($response, 201);
    }

    public function destroy($id) {
        $row = $this->model->own()->findOrFail($id);
        if ($row->delete()) {
            return response()->json([
                'message' => trans('api.Deleted successfully'),
            ], 200);
        }
        return response()->json(['message' => trans('api.Failed to delete')], 400);
    }

}

<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use Auth;

class Authenticate extends Middleware {
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return string|null
     */
    public function handle($request, Closure $next) {
        ///////// check if request is not api
        if ($request->segment(1)!='api') {
            return redirect('/');
        } else {
            $token=token();
            if ($token) {
                $row = \App\Models\User::whereHas('tokens', function ($query) use ($token) {
                    $query->where('token', $token);
                })->first();
                if (!$row) {
                    return response()->json(['message' => trans('api.Unauthorized user')], 401);
                }
                \Auth::login($row);
                return $next($request);
            }
            else{
                return response()->json(['message' => trans('api.Unauthorized user')], 401);
            }
        }
    }
}

<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        if (Auth::guard($guard)->check()) {
            flash()->success(trans('admin.User is already logged in'));
            if(auth()->user()->role_id)
                return redirect(lang().'/admin/dashboard');
            else
                return redirect(lang().'/');
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RatesResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        $row=[
            'type' => 'rates',
            'id' => $this->id,
            'attributes' => [
                'id' => $this->id,
                'user_id' => $this->user_id,
                'movie_id' => $this->movie_id,
                'movie_title' => $this->movie_title,
                'movie_img' => $this->movie_img,
                'value'=>$this->value,
                'created_at' =>date('Y-m-d',strtotime($this->created_at))
            ]
        ];
        return $row;
    }

}

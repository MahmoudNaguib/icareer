<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        $row=[
            'type' => 'users',
            'id' => $this->id,
            'attributes' => [
                'id' => $this->id,
                'name' => $this->name,
                'email' => $this->email,
                'mobile' => $this->mobile,
                'last_logged_in_at' => $this->last_logged_in_at,
                'created_at' =>date('Y-m-d',strtotime($this->created_at))
            ],
            'relationships' => [
            ],
            'token'=>$this->when(request()->header('Authorization'),request()->header('Authorization')),
        ];
        if(token()){
            $row['token']=token();
        }
        return $row;
    }

}

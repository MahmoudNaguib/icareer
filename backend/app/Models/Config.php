<?php

namespace App\Models;

class Config extends BaseModel {
    protected $table = "configs";
    protected $guarded = [
    ];
    protected $hidden = [
    ];

    public function getConfigs() {
        $arr = [];
        $configs = $this->get();
        if ($configs) {
            foreach ($configs as $c) {
                $key = $c->field;
                $arr[$key] = $c->value;
            }
        }
        $arr['version'] = appVersion();
        return $arr;
    }
}

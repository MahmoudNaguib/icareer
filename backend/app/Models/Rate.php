<?php

namespace App\Models;

class Rate extends BaseModel {

    protected $table = "rates";
    protected $guarded = [
    ];
    protected $hidden = [
    ];
    public $rules = [
        'user_id' => 'required',
        'value'=>'required|numeric',
        'movie_id' => 'required',
        'movie_title' => 'required',
        'movie_img' => 'required',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeOwn($query) {
        $id = auth()->user()->id;
        return $query->where('user_id', $id);
    }

}

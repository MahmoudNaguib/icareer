<?php

namespace App\Models;

class Token extends BaseModel {

    protected $table = "tokens";
    protected $guarded = [
    ];
    protected $hidden = [
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

}

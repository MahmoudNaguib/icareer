<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Authenticatable {
    use SoftDeletes,
        HasFactory;

    protected $attributes = [
        'confirmed' => 0,
    ];
    protected $table = "users";
    protected $guarded = [
        'deleted_at',
    ];
    protected $hidden = [
        'password',
        'remember_token',
        'confirm_token',
        'confirmed',
        'updated_at',
        'deleted_at',
    ];
    public $loginRules = [
        'email' => 'required|email',
        'password' => 'required|min:8',
        'push_token' => 'nullable|min:4',
        'device'=>'nullable|min:3'
    ];

    public function tokens() {
        return $this->hasMany(Token::class);
    }

    public function setPasswordAttribute($value) {
        if (trim($value)) {
            $this->attributes['password'] = bcrypt(trim($value));
        }
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\JsonResource;
use Form;
use Validator;
use Config;
use Illuminate\Pagination\Paginator;


class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Schema::defaultStringLength(190);
        /// validation rules
        Validator::extend('mobile', function ($attribute, $value, $parameters, $validator) {
            if ($value == '') {
                return true;
            }
            if (!trim($value) && intval($value) != 0) {
                return true;
            }
            return is_numeric($value);
        });
        Validator::extend('phone', function ($attribute, $value, $parameters, $validator) {
            if ($value == '') {
                return true;
            }
            if (!trim($value) && intval($value) != 0) {
                return true;
            }
            return (preg_match('/^([(+)0-9,\\-,+,]){4,15}$/', $value));
        });
        Paginator::useBootstrap();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}

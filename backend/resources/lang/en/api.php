<?php 

return [
    'Invalid input data' => 'Invalid input data',
    'There is no account with this email' => 'There is no account with this email',
    'This account is not confirmed' => 'This account is not confirmed',
    'Please check your email to confirm your account' => 'Please check your email to confirm your account',
    'Trying to login with invalid password' => 'Trying to login with invalid password',
    'Successfully logged in' => 'Successfully logged in',
    'Failed to login' => 'Failed to login',
    'Log out successfully' => 'Log out successfully',
    'Unauthorized user' => 'Unauthorized user',
    'This movie is already in your favourites' => 'This movie is already in your favourites',
    'Added successfully' => 'Added successfully',
    'Deleted successfully' => 'Deleted successfully',
    'Failed to delete' => 'Failed to delete',
    'This movie is already rated' => 'This movie is already rated',
    'Rated successfully' => 'Rated successfully',
];
<?php
Route::resource('configs', 'Api\ConfigsController');
Route::group(['middleware' => ['guest']], function () {
    Route::resource('login', 'Api\Auth\LoginController');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('profile', 'Api\Logged\ProfileController');
    Route::resource('logout', 'Api\Logged\LogoutController');
    Route::resource('favourites', 'Api\Logged\FavouritesController');
    Route::resource('rates', 'Api\Logged\RatesController');
});

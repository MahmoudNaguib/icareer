<?php
namespace Tests\Feature;
use Database\Seeders\DatabaseSeeder;

trait DefaultData{
    public $adminUser;
    public function setup():void{
        parent::setup();
        $this->seed(DatabaseSeeder::class);
        $this->apiURL='api';
        $this->user=\App\Models\User::where('role_id','=',null)->first();
        $this->apiToken= \App\Models\Token::where('user_id',$this->user->id)->first();
    }
    public function actingAsUserApi() {
        $this->actingAs($this->user)->withHeaders(['token' => $this->apiToken->token]);
    }

}

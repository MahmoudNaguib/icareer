export default function ({store,app,redirect}) {
  if(store.state.auth.loggedIn) {
    return redirect('/');
  }
}

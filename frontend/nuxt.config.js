export default {
  target: 'server',
  // Global page headers (https://go.nuxtjs.dev/config-head)
  publicRuntimeConfig: {
    BASE_URL: process.env.BASE_URL,
    BACKEND_URL: process.env.BACKEND_URL,
    MOVIES_URL: process.env.MOVIES_URL,
    API_KEY: process.env.API_KEY,
    IMAGE_SRC: process.env.IMAGE_SRC,
  },
  privateRuntimeConfig: {},
  head: {
    //  titleTemplate: process.env.APP_TITLE+':%s',
    htmlAttrs: {
      lang:'en', //this.$i18n.locale
      dir: 'ltr'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {name: 'google-site-verification', content: '2ky_EVfIhCmJcCKBhW0PsYxIaoeueWwAiMRnnSN5V9U'},
      {hid: 'description', name: 'description', content: ''},
      {hid: 'og:locale', property: 'og:locale', content: 'en_EG'},
      {hid: 'og:type', property: 'og:type', content: 'website'},
      {hid: 'og:url', property: 'og:url', content: process.env.BASE_URL},
      {hid: 'og:site_name', property: 'og:site_name', content: 'ICareer'},
      {hid: 'fb:app_id', property: 'fb:app_id', content: '2417803741600196'},
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: "stylesheet", type: "text/css", href: process.env.BASE_URL + "/css/bootstrap.min.css"},
      {rel: "stylesheet", type: "text/css", href: process.env.BASE_URL + "/css/font-awesome.min.css"},
      {rel: "stylesheet", type: "text/css", href: process.env.BASE_URL + "/css/style.css"},
    ],
    script: [
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    //'assets/css/bootstrap.min.css'
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '~/plugins/axios.js',
    '~/plugins/helpers.js',
    '~/plugins/meta.js',
    { src: '~/plugins/vue-confirm-dialog.js', ssr: false },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/moment',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/toast',
  ],
  toast: {
    position: 'bottom-right',
    duration: 5000,
  },

  auth: {
    plugins: ['~/plugins/auth.js'],
    strategies: {
      local: {
        endpoints: {
          login: {url: `login`, method: 'post', propertyName: 'data.token'},
          user: {url: `profile`, method: 'get', propertyName: 'data'},
          logout: {url: 'logout', method: 'get'},
        },
        autoFetchUser:false
      }
    },
  },
  router: {
    middleware: ['resetBreadcrumb']
  },
  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: process.env.BACKEND_URL + '/api/'
  },
  loading: '~/components/LoadingBar.vue',
  loadingIndicator: {
    name: 'circle',
    color: '#3B8070',
    background: 'white'
  }
  // Build Configuration (https://go.nuxtjs.dev/config-build)
}

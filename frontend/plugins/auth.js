export default function({app, $auth }) {
  $auth.options.redirect = {
    login: '/',
    logout: '/login',
    callback: '/',
    home: '/'
  };
}

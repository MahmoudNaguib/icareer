export default function ({$axios, app, store, route}) {
  $axios.onRequest(config => {
    store.commit("setIsSubmitting", true);
    store.commit("setErrors", []);
    //console.log(config.url);
    console.log('Making request to ' + config.url);

  });
  $axios.onError(e => {
    store.commit("setIsSubmitting", false);
    if (e.response.data.errors != undefined) {
      store.commit("setErrors", e.response.data.errors);
    }else if(e.response.data.message != undefined){
      app.$toast.error(e.response.data.message);
    }
  });
  $axios.onResponse(res => {
    //console.log(res.data.data[0]);
    if (res.data.message != undefined) {
      app.$toast.success(res.data.message);
    }
    store.commit("setIsSubmitting", false);
  });
}

const helpers = {
  resetForm(form,def='') {
    Object.keys(form).forEach(key => {
      form[key] = [];
    });
    if(def !='') {
      Object.keys(def).forEach(key => {
        form[key] = def[key];
      });
    }
  },
  rateClass(rate,n) {
    let styleClass;
    styleClass=(Math.ceil(rate)>=n)?`fas`:`far`;
    styleClass+=' fa-star';
    styleClass+=(Math.ceil(rate)>=n && rate<n)?'-half-alt':'';
    return styleClass;
  }
}

export default ({ app }, inject) => {
  inject('helpers', helpers);
}


export default function ({app,store},inject) {
  inject('prepareMeta',function (payload,context){
    let title=context.$store.state.configs.app_name;
    if(payload.title!=undefined){
      title+=" : "+payload.title;
    }
    ///////// description
    let description=context.$store.state.configs.meta_description;
    if(payload.description){
      description=payload.description;
    }
    ///////// keywords
    let keywords=context.$store.state.configs.meta_keywords;
    if(payload.keywords){
      keywords=payload.keywords;
    }
    ///////// image
    let image;
    let defImage=process.env.BACKEND_URL + '/uploads/large/'+context.$store.state.configs.logo;
    if(payload.image){
      image=payload.image;
    }
    else{
      image=defImage;
    }
    ///////// url
    let url=process.env.BASE_URL + context.$route.path;
    if(payload.url){
      url=payload.url;
    }
    let response={
      title:title,
      meta:[
        {
          hid: 'description',
          name: 'description',
          content: description
        },
        {
          hid: 'keywords',
          name: 'keywords',
          content: keywords
        },
        // Open Graph
        {
          hid: 'og:url',
          property: 'og:url',
          content: process.env.BASE_URL + '/' + this.$route.path
        },
        {
          hid: 'og:image',
          property: 'og:image',
          content: image
        },
        {
          hid: 'og:title',
          property: 'og:title',
          content: title
        },
        {
          hid: 'og:description',
          property: 'og:description',
          content: description
        },
        // Twitter Card
        {
          hid: 'twitter:title',
          name: 'twitter:title',
          content: title
        },
        {
          hid: 'twitter:description',
          name: 'twitter:description',
          content: description
        }
      ]
    };
    return response;
  });
}

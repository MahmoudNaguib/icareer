export default {
  state: () => ({
    isSubmitting: false,
    errors: [],
    configs: {},
    breadcrumb: {}
  }),
  mutations: {
    setIsSubmitting(state, payload) {
      state.isSubmitting = payload;
    },
    setErrors(state, payload) {
      state.errors = payload;
    },
    setBreadcrumb(state, payload) {
      state.breadcrumb = payload;
    },
    setConfigs(state, payload) {
      state.configs = payload;
    },
    setMovies(state, payload) {
      state.movies = payload;
    },
    setMovie(state, payload) {
      state.movie = payload;
    },
    //////////////////// Favourites
    setFavourites(state, payload) {
      state.favourites = payload;
    },
    addFavourite(state, payload) {
      state.favourites.push(payload);
    },
    deleteFavourite(state, id) {
      let index = state.favourites.findIndex(c => c.id == id);
      if (index != -1) {
        state.favourites.splice(index, 1);
      }
    },
    //////////////////// Rates
    setRates(state, payload) {
      state.rates = payload;
    },
    addRate(state, payload) {
      state.rates.push(payload);
    },
    deleteRate(state, id) {
      let index = state.rates.findIndex(c => c.id == id);
      if (index != -1) {
        state.rates.splice(index, 1);
      }
    },
  },

  actions: {
    async nuxtServerInit({commit, dispatch}, {app}) {
      let configs = await this.$axios.get(`/configs`);
      commit('setConfigs', configs.data.data);
      if (this.$auth.loggedIn) {
        let favourites = await this.$axios.get(`/favourites`);
        if (favourites.data != undefined) {
          commit('setFavourites', favourites.data.data);
        }
        let rates = await this.$axios.get(`/rates`);
        if (rates.data != undefined) {
          commit('setRates', rates.data.data);
        }

      }
    },
  }
}
